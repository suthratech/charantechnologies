	if ($('.sponsors-slider').length) {

		$('.sponsors-slider').owlCarousel({
			loop:true,
			margin:30,
			nav:true,
			smartSpeed: 2000,
			autoplay: 2000,
			navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
			responsive:{
				0:{
					items:1
				},
				400:{
					items:2
				},
				600:{
					items:3
				},
				800:{
					items:4
				},
				1000:{
					items:5
				},
				1200:{
					items:5
				}
			}
		});  
		
		$('.sponsors-slider').on('mouseenter',function(e){
			// console.log('qwerty');
			$(this).closest('.owl-carousel').trigger('stop.owl.autoplay');
		});
		$('.sponsors-slider').on('mouseleave',function(e){
			// console.log('pavan'); 
			$(this).closest('.owl-carousel').trigger('play.owl.autoplay');
		});
	}
	