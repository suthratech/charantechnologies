<?php
$field_name = $_POST['cname'];
$field_email = $_POST['cemail'];
$field_phone = $_POST['cphone'];
$field_subject = $_POST['csubject'];
$field_message = $_POST['cmessage'];

$mail_to = 'enquiry@bhavishyadevelopers.in';
$subject = 'Message from a site visitor '.$field_name;

$body_message = 'From: '.$field_name."\n";
$body_message .= 'E-mail: '.$field_email."\n";
$body_message .= 'Phone: '.$field_phone."\n";
$body_message .= 'Subject: '.$field_subject."\n";
$body_message .= 'Message: '.$field_message;

$headers = 'From: '.$field_email."\r\n";
$headers .= 'Reply-To: '.$field_email."\r\n";

$mail_status = mail($mail_to, $subject, $body_message, $headers);

if ($mail_status) { ?>
    <script language="javascript" type="text/javascript">
        alert('Thank you for the message. We will contact you shortly.');
        window.location = 'contact.html';
    </script>
<?php
}
else { ?>
    <script language="javascript" type="text/javascript">
        alert('Message failed.');
        window.location = 'contact.html';
    </script>
<?php
}
?>

